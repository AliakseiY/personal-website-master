export function createCommunitySection() {
    window.addEventListener('load', () => {
    

      const membersDiv = document.querySelector('.app-community-members');

      createGetRequest((records) => {
        const membersInfoHtml = printMembersInfo(records);
  
        // eslint-disable-next-line
              for (const member of membersInfoHtml) {
          membersDiv.appendChild(createMemberDiv(member));
        }
      });
    });
  }
  
  function createGetRequest(callback) {
    const xhr = new XMLHttpRequest();
  
    xhr.open('GET', '/api/community', true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
    xhr.send();
    // eslint-disable-next-line
      xhr.addEventListener('readystatechange', () => {
      if (xhr.readyState === 4) {
        callback(xhr.responseText);
      }
    });
  }
  
  function createMemberDiv(html) {
    const memberDiv = document.createElement('div');
    memberDiv.classList = 'app-section-member';
    memberDiv.innerHTML = html;
    return memberDiv;
  }
  
  function printMembersInfo(members) {
    members = JSON.parse(members);
    return members.map(member => createMemberCard(member));
  }
  
  function createMemberCard(member) {
    const review = member.review || 'PLACEHOLDER: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do incididunt ut labore et dolor.';
    const cardHtml = `
  <img src='${member.avatar}' class="app-section__member-img"/>
  <p class="app-section__member-review">
  ${review}
  </p>
  <div class="app-section__member-name">
  ${member.firstName} ${member.lastName}
  </div>
  <div class="app-section__member-position">
  ${member.position}
  </div>`;
    return cardHtml;
  }
  