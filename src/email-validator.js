function validate(emailAddress) {
    const VALID_EMAIL_ENDING = ['gmail.com', 'outlook.com', 'yandex.ru'];

    const email = emailAddress.substring(emailAddress.indexOf('@') + 1);

    const emailSet = new Set(VALID_EMAIL_ENDING);
    return emailSet.has(email);
}

export { validate }; // eslint-disable-line
