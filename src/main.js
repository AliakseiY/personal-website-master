import './styles/normalize.css';
import './styles/style.css';
import CreateSection from "./join-us-section.js"; //eslint-disable-line
import {createCommunitySection} from "./community.js"; //eslint-disable-line

createCommunitySection();

const section = new CreateSection;

section.standard();
