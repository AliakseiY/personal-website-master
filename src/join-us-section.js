import {validate} from "./email-validator.js"; 
export default class CreateSection {


    sectionHtml = null; //eslint-disable-line


    standard(){
    let newHtmlSection = document.createElement("section");
    newHtmlSection.className = "app-section app-section--image-subscribe standard-app-section"

    // Title section

    let sectionTitle = document.createElement("h2")
    let sectionTitleText = document.createTextNode("Join Our Program")
    sectionTitle.appendChild(sectionTitleText)
    sectionTitle.className = "app-title"
    newHtmlSection.appendChild(sectionTitle)

    // Subtitle section

    let sectionSubtitle = document.createElement("h3")
    let sectionSubtitleText = document.createTextNode("Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
    sectionSubtitle.appendChild(sectionSubtitleText)
    sectionSubtitle.className = "app-subtitle"
    newHtmlSection.appendChild(sectionSubtitle)

    // Form section

    let sectionForm = document.createElement("form")
    sectionForm.className = "app-form"

    // Form Input Field

    let formInputField = document.createElement("input")
    formInputField.className = "app-section__email"
    formInputField.placeholder = "Email"
    sectionForm.appendChild(formInputField)

    // Form Button

    let formButton = document.createElement("button")
    formButton.className = "app-section__button app-section__button--subscribe"
    formButton.type = "submit"
    let formButtonTitle = document.createTextNode(window.localStorage.getItem('button'));
    formButton.appendChild(formButtonTitle)
    sectionForm.appendChild(formButton)

    sectionForm.addEventListener('submit', function(event) {
        //Blocking default behavior of the submit button
        event.preventDefault();
        
        formButton.disabled = true;
        window.setTimeout(sendRequest, 3000);

        function sendRequest(){
        var newButtonText = (window.localStorage.getItem("button")=="Subscribe")? "Unsubscribe" : "Subscribe";

        if(window.localStorage.getItem("button")=="Unsubscribe"){
            var unsubEmail = window.localStorage.getItem('email');

            formButton.innerText=newButtonText;
            window.localStorage.setItem('button',newButtonText);
            window.localStorage.removeItem('email');
            formInputField.className = "app-section__email";

            //Unsubscribe POST call
              
                const body = {email: `${unsubEmail}`};
                const xhr = new XMLHttpRequest();
              
                xhr.open('POST', '/api/unsubscribe', true);
                xhr.setRequestHeader('Content-type', 'application/json');
                xhr.send(JSON.stringify(body));
                // eslint-disable-next-line
                xhr.addEventListener('readystatechange', () => {
                  if (xhr.readyState === 4) {
                    formButton.disabled = false;
                   console.log("User has been Unsubscribed");
                  }
                });
              

        } else if (validate(formInputField.value)){
            
            const body = {email: `${formInputField.value}`};

            window.localStorage.setItem('button',newButtonText);
            formButton.innerText=newButtonText;
            window.localStorage.setItem("email",formInputField.value);
            formInputField.value = "";
            
            formInputField.className = "app-section__email hidden" ;
            //Subscribe POST call
                
                const xhr = new XMLHttpRequest();

                xhr.open('POST', '/api/subscribe', true);
                xhr.setRequestHeader('Content-type', 'application/json');

                xhr.send(JSON.stringify(body));
                // eslint-disable-next-line
                 xhr.addEventListener('readystatechange', () => {
                     if(xhr.readyState === 4){
                        formButton.disabled = false;
                         if (xhr.status == 422) {
                            console.log(xhr.responseText);
                        } else if(xhr.status!=200){
                            alert("Error: "+xhr.responseText);
                        }
                }
            });
              

        } else {
            alert("Wrong email.");
        }
    }
    });

    //adding form to the section
    newHtmlSection.appendChild(sectionForm)
    document.addEventListener("DOMContentLoaded", function() {
        //adding the whole section built above right before the footer
        let footer = document.getElementsByClassName('app-footer')[0];
        footer.parentNode.insertBefore(newHtmlSection, footer);

        if(window.localStorage.getItem("button")==null){
            window.localStorage.setItem("button", "Subscribe");
        }

        if(window.localStorage.getItem("button")=="Unsubscribe"){
            formInputField.className = "app-section__email hidden" ;
        }
    });
    }

    advanced (){
        let newHtmlSection = document.createElement("section");
        newHtmlSection.className = "app-section app-section--image-subscribe advanced-app-section"
    
        // Title section
    
        let sectionTitle = document.createElement("h2")
        let sectionTitleText = document.createTextNode("Join Our Advanced Program")
        sectionTitle.appendChild(sectionTitleText)
        sectionTitle.className = "app-title"
        newHtmlSection.appendChild(sectionTitle)
    
        // Subtitle section
    
        let sectionSubtitle = document.createElement("h3")
        let sectionSubtitleText = document.createTextNode("Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
        sectionSubtitle.appendChild(sectionSubtitleText)
        sectionSubtitle.className = "app-subtitle"
        newHtmlSection.appendChild(sectionSubtitle)
    
        // Form section
    
        let sectionForm = document.createElement("form")
        sectionForm.className = "app-form"
    
        // Form Input Field
    
        let formInputField = document.createElement("input")
        formInputField.className = "app-section__email"
        formInputField.placeholder = "Email"
        sectionForm.appendChild(formInputField)
    
        // Form Button
    
        let formButton = document.createElement("button")
        formButton.className = "app-section__button app-section__button--subscribe"
        formButton.type = "submit"
        let formButtonTitle = document.createTextNode("Subscribe to Advanced Program")
        formButton.appendChild(formButtonTitle)
        sectionForm.appendChild(formButton)
    
        sectionForm.addEventListener('submit', function(event) {
               //Blocking default behavior of the submit button
               event.preventDefault();
               //print value from the field into console log
               console.log(formInputField.value);
        });
    
        //adding form to the section
        newHtmlSection.appendChild(sectionForm)
        document.addEventListener("DOMContentLoaded", () => {
            //adding the whole section built above right before the footer
            let footer = document.getElementsByClassName('app-footer')[0];
            this.sectionHtml = footer.parentNode.insertBefore(newHtmlSection, footer);
      
        });
        }
        //adding removing option
    removed(sectionClassName){
        // var section = document.getElementsById(sectionClassName);
        // section[0].parentNode.removeChild(section[0]);
        this.sectionHtml.remove();
    }
}
