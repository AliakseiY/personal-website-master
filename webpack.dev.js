const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require("path");

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    proxy: {
      '/api': {
          target: 'http://localhost:3000',
          pathRewrite: { '^/api': '' },
      },
    },
    contentBase: path.join(__dirname, "dist"),
    hot: true
  },
});