module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": 'airbnb-base',
    "globals": {
            "Atomics": "readonly",
            "SharedArrayBuffer": "readonly"
        },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "no-undef": 2,
        "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
        "default-case": 2,
        'func-name': 'off',
        "no-alert": "error",
        'max-classes-per-file': ['error', 3],
        "no-console": ["error", { allow: ["warn", "error"] }],
        'space-before-blocks': 'warn',
        'indent': ['error', 4],
        'new-parens': ['error', 'never']
    }
}
